## Voice conversion using neural networks

This repository is part of my engineer thesis, which was done in collaboration with my colleague from univeristy. Everything you find in this repository was done by me. In thesis (sadly, only in polish), which can be found in master branch, you can see distribution of work. I was responsible for convolutional neural networks and seq2seq models.

Voice conversion system aims to modify spoken utterances of one speaker in such way that it sounds like someone�s else. In this repository you can find two branches which consist of Python code of two different aproaches to the problem.
In the branch named SimpleNN, there are Python scripts for creating convolutional neural networks. It also consists of google colab notebooks which was used to compute models.
In the branch named seq2seq, I have put Python scipts made with my thesis supervisor, used for seq2seq models, which came out to not work with data we prepared. 
Data we used comes from [CMU Arctic databases](http://www.festvox.org/cmu_arctic/), which consist of around 1150 utterances of 6 different speakers.
